class House:
    def __init__(self, owner):
        self.rooms = []
        self.owner = owner

    def __str__(self):
        size = len(self.rooms)
        if size < 3:
            sizeTerm = "hut"
        elif size < 10:
            sizeTerm = "house"
        elif size < 40:
            sizeTerm = "mansion"
        else:
            sizeTerm = "palace"
        return self.owner + "'s " + sizeTerm.capitalize()