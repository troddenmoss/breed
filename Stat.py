class Stat:
    def __init__(self, base):
        self.current = base
        self.max = base

    def get(self):
        return self.current

    def modify(self, value):
        self.current = self.current + value

# class Fertility(Stat):
