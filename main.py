#!/usr/bin/python3
import Character


def main():
    pc = Character.load("lea")
    actors = [pc]
    beginTurn(pc)


def beginTurn(actor: Character):
    print(actor)
    print(actor.house)


if __name__ == "__main__":
    main()
