import sys
sys.path.append(".")

import Character, House

def create():
    obj = Character.Character()
    obj.name = "Lea"
    obj.age = 18
    obj.fertility = 70
    obj.virility = 50
    obj.energy = 100
    obj.house = House.House(obj.name)
    obj.inventory = Character.loadout()
    return obj

#print(create(Character.Character()))